package com.example.piotr.virondemo.business.sign_up

import io.reactivex.Completable

interface SignUpInteractor {

    fun validateLogin(login: String): Boolean

    fun validatePassword(password: String): Boolean

    fun signUp(email: String,
               password: String): Completable
}
