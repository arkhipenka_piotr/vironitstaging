package com.example.piotr.virondemo.ui.base

import android.content.DialogInterface
import android.view.View
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import com.example.piotr.virondemo.utils.AddToEndSingleByTagStateStrategy

interface MessageView {

    @StateStrategyType(AddToEndSingleStrategy::class)
    fun showAutocloseableMessage(message: String)

    @StateStrategyType(AddToEndSingleByTagStateStrategy::class, tag = "dialog_message")
    fun showDialogMessage(message: String, closable: Boolean)

    @StateStrategyType(AddToEndSingleByTagStateStrategy::class, tag = "dialog_message")
    fun showDialogWithOptions(title: String, message: String,
                              positiveOptionMessage: String?, negativeOptionMessage: String?,
                              positiveListener: DialogInterface.OnClickListener?,
                              negativeListener: DialogInterface.OnClickListener?,
                              cancelable: Boolean)

    @StateStrategyType(AddToEndSingleByTagStateStrategy::class, tag = "message")
    fun showMessage(message: String, closable: Boolean,
                    actionMessage: String?, actionListener: View.OnClickListener?)

    @StateStrategyType(AddToEndSingleByTagStateStrategy::class, tag = "dialog_message")
    fun hideDialogMessage()

    @StateStrategyType(AddToEndSingleByTagStateStrategy::class, tag = "message")
    fun hideMessage()
}