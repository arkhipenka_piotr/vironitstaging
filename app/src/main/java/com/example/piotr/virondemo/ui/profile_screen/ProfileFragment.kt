package com.example.piotr.virondemo.ui.profile_screen

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.*
import android.view.animation.AnimationUtils
import android.widget.Toast
import com.arellomobile.mvp.MvpAppCompatFragment
import com.arellomobile.mvp.presenter.InjectPresenter
import com.bumptech.glide.Glide
import com.example.piotr.virondemo.R
import com.example.piotr.virondemo.model.User
import com.example.piotr.virondemo.ui.base.BaseFragment
import com.example.piotr.virondemo.ui.login_screen.LoginActivity
import kotlinx.android.synthetic.main.content_profile.*
import kotlinx.android.synthetic.main.fragment_profile.*
import java.io.File

class ProfileFragment : BaseFragment(), ProfileView {

    override val layoutResId: Int
        get() = R.layout.fragment_profile

    @InjectPresenter
    lateinit var profilePresenter: ProfilePresenter

    companion object {
        private const val IMAGE_PICKER_REQUEST_CODE = 1
        const val KEY = "ProfileFragment"
        fun newInstance() = ProfileFragment()
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        super.onCreateOptionsMenu(menu, inflater)

        inflater!!.inflate(R.menu.menu_profile, menu)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        if (activity != null) {
            activity!!.setTitle(R.string.profile)
        }

        (activity as AppCompatActivity).setSupportActionBar(profileToolbar)

        return super.onCreateView(inflater, container)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        showSaveButton()
        button_save.setOnClickListener {
            profilePresenter.saveUserAndExit(first_name_text.text.toString(),
                    last_name_text.text.toString(),
                    email_text.text.toString())
        }

        bt_exit.setOnClickListener {
            profilePresenter.exitFromAccount()
        }

        //TODO
        profileToolbar.setOnClickListener {

        }
    }

    override fun bindUserData(user: User?) {
        if (user != null) {
            first_name_text.setText(user.firstName)
            last_name_text.setText(user.lastName)
            email_text.setText(user.email)
        }
    }

    override fun updateImage(imageUrl: String?) {
        Glide.with(this)
                .load(imageUrl)
                .into(profileImage)
    }

    override fun updateImage(file: File) {
        Glide.with(this)
                .load(file)
                .into(profileImage!!)
    }

    override fun exit() {
        if (activity != null && activity!!.supportFragmentManager != null) {
            activity!!.supportFragmentManager
                    .popBackStack()
        }
    }

    private fun showSaveButton() {
        val saveButtonAnimation = AnimationUtils.loadAnimation(context, android.R.anim.slide_in_left)
        button_save.visibility = View.VISIBLE
        saveButtonAnimation.duration = 700
        button_save.startAnimation(saveButtonAnimation)
    }

    //TODO
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
    }

    override fun moveToSignIn() {
        if (activity != null) {
            val intent = Intent(context, LoginActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            activity!!.startActivity(intent)
        }
    }
}
