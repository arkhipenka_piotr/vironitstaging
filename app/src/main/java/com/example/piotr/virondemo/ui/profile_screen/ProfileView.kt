package com.example.piotr.virondemo.ui.profile_screen

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy
import com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import com.example.piotr.virondemo.model.User
import com.example.piotr.virondemo.ui.base.BaseView
import com.example.piotr.virondemo.utils.AddToEndSingleByTagStateStrategy
import java.io.File

interface ProfileView : BaseView {

    @StateStrategyType(AddToEndSingleStrategy::class)
    fun bindUserData(user: User?)

    @StateStrategyType(AddToEndSingleStrategy::class)
    fun exit()

    @StateStrategyType(AddToEndSingleStrategy::class)
    fun updateImage(imageUrl: String?)

    @StateStrategyType(AddToEndSingleStrategy::class)
    fun updateImage(file: File)

    @StateStrategyType(OneExecutionStateStrategy::class)
    fun moveToSignIn()
}
