package com.example.piotr.virondemo.dagger.components

import com.example.piotr.virondemo.App
import com.example.piotr.virondemo.business.data.firebase_database.FirebaseDatabaseRepositoryImpl
import com.example.piotr.virondemo.business.data.firebase_storage.FirebaseStorageRepositoryImpl
import com.example.piotr.virondemo.business.data.user_preferences.UserPreferencesRepositoryImpl
import com.example.piotr.virondemo.business.login.LoginInteractorImpl
import com.example.piotr.virondemo.business.profile.ProfileInteractorImpl
import com.example.piotr.virondemo.business.sign_up.SignUpInteractorImpl
import com.example.piotr.virondemo.dagger.modules.*
import com.example.piotr.virondemo.resources.ResourcesManagerImpl
import com.example.piotr.virondemo.ui.home_screen.HomePresenter
import com.example.piotr.virondemo.ui.login_screen.LoginPresenter
import com.example.piotr.virondemo.ui.profile_screen.ProfilePresenter
import com.example.piotr.virondemo.ui.sign_up_screen.SignUpPresenter
import com.example.piotr.virondemo.ui.splash_screen.SplashPresenter
import dagger.BindsInstance
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [BusinessModule::class, ContextModule::class, DataModule::class, FirebaseModule::class, SchedulersModule::class, AppActivitiesModule::class])
interface AppComponent {

    fun inject(loginInteractor: LoginInteractorImpl)

    fun inject(loginPresenter: LoginPresenter)

    fun inject(signUpInteractor: SignUpInteractorImpl)

    fun inject(signUpPresenter: SignUpPresenter)

    fun inject(userPreferencesRepository: UserPreferencesRepositoryImpl)

    fun inject(profileInteractor: ProfileInteractorImpl)

    fun inject(homePresenter: HomePresenter)

    fun inject(profilePresenter: ProfilePresenter)

    fun inject(firebaseDatabaseRepository: FirebaseDatabaseRepositoryImpl)

    fun inject(firebaseStorageRepository: FirebaseStorageRepositoryImpl)

    fun inject(resourcesManagerImpl: ResourcesManagerImpl)

    fun inject(app: App)

    fun inject(splashPresenter: SplashPresenter)

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun appContext(app: App): Builder
        fun contextModule(contextModule: ContextModule): Builder

        fun build(): AppComponent
    }
}
