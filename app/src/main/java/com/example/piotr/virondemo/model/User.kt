package com.example.piotr.virondemo.model

import java.io.Serializable

data class User(val id: String? = null,
                var firstName: String? = null,
                var lastName: String? = null,
                var email: String? = null,
                var photoUrl: String? = null) : Serializable