package com.example.piotr.virondemo.business.data.firebase_storage

import com.example.piotr.virondemo.App
import com.google.firebase.storage.FirebaseStorage
import durdinapps.rxfirebase2.RxFirebaseStorage
import io.reactivex.Single
import javax.inject.Inject

class FirebaseStorageRepositoryImpl : FirebaseStorageRepository {

    @Inject
    lateinit var firebaseStorage: FirebaseStorage

    init {
        App.getAppComponent()
                .inject(this)
    }
    companion object {
        private const val IMAGE_PATH = "images/"
    }

    override fun loadImage(userId: String, byteArray: ByteArray): Single<String> {
        val storageReference = firebaseStorage.reference.child(IMAGE_PATH + userId)
        return RxFirebaseStorage.putBytes(storageReference, byteArray)
                .map {
                    it.metadata?.downloadUrl.toString()
                }
                .toSingle()
    }
}