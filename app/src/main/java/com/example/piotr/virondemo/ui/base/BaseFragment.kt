package com.example.piotr.virondemo.ui.base

import android.app.Activity
import android.app.Fragment
import android.content.Context
import android.content.DialogInterface
import android.os.Bundle
import android.os.Handler
import android.support.annotation.IdRes
import android.support.annotation.LayoutRes
import android.support.design.widget.BaseTransientBottomBar
import android.support.design.widget.Snackbar
import android.support.v4.content.ContextCompat
import android.support.v7.app.AlertDialog
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import com.arellomobile.mvp.MvpAppCompatFragment
import com.example.piotr.virondemo.R
import com.example.piotr.virondemo.ui.profile_screen.ProfileView
import com.example.piotr.virondemo.utils.*
import dagger.multibindings.StringKey
import hugo.weaving.DebugLog

abstract class BaseFragment : MvpAppCompatFragment(), BaseView {

    private var mSnackBar: Snackbar? = null

    private var mRootActivityBaseView: BaseView? = null

    @get:LayoutRes
    abstract val layoutResId: Int

    override fun onAttach(context: Context?) {
        super.onAttach(context)

        if (context is BaseView) {
            mRootActivityBaseView = context
        }
    }

    open fun initArguments(args: Bundle) {
        AppLog.logFragment(this)
    }


    open fun initBeforeLayout() {
        AppLog.logFragment(this)
        if (arguments != null) {
            initArguments(arguments!!)
        }
    }

    @DebugLog
    override fun onStop() {
        super.onStop()
    }

    @DebugLog
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return onCreateView(inflater, container)
    }

    @DebugLog
    override fun onPause() {
        super.onPause()
    }

    @DebugLog
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        initBeforeLayout()
        if (savedInstanceState != null) {
            initArguments(savedInstanceState)
        }
        super.onViewCreated(view, savedInstanceState)
    }

    @DebugLog
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
    }

    @DebugLog
    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return super.onOptionsItemSelected(item)
    }

    @DebugLog
    override fun onDestroyView() {
        hideMessage()
        hideProgress()
        super.onDestroyView()
    }

    override fun onDetach() {
        if (mRootActivityBaseView != null) {
            mRootActivityBaseView = null
        }
        super.onDetach()
    }

    fun onCreateView(inflater: LayoutInflater, container: ViewGroup?): View? {
        initBeforeLayout()
        return inflater.inflate(layoutResId, container, false)
    }

    override fun cancelScreen() {
        activity?.onBackPressed()
    }

    override fun showProgress() {
        mRootActivityBaseView?.showProgress()
    }

    override fun showProgress(messageText: String?) {
        mRootActivityBaseView?.showProgress(messageText)
    }

    override fun hideProgress() {
        mRootActivityBaseView?.hideProgress()
    }

    override fun showAutocloseableMessage(message: String) {
        showMessage(message, true, null, null)
    }

    override fun showDialogMessage(message: String, closable: Boolean) {
        mRootActivityBaseView?.showDialogMessage(message, closable)
    }

    override fun showDialogWithOptions(title: String, message: String, positiveOptionMessage: String?, negativeOptionMessage: String?, positiveListener: DialogInterface.OnClickListener?, negativeListener: DialogInterface.OnClickListener?, cancelable: Boolean) {
        hideDialogMessage()
        mRootActivityBaseView?.showDialogWithOptions(title, message, positiveOptionMessage, negativeOptionMessage, positiveListener, negativeListener, cancelable)
    }

    override fun showMessage(message: String, closable: Boolean, actionMessage: String?, actionListener: View.OnClickListener?) {
        hideMessage()
        val duration = if (closable) BaseTransientBottomBar.LENGTH_SHORT else BaseTransientBottomBar.LENGTH_INDEFINITE
        mSnackBar = showSnackBar(message, actionMessage, actionListener, duration)
    }

    override fun hideDialogMessage() {
        mRootActivityBaseView?.hideDialogMessage()
    }


    override fun hideMessage() {
        if (mSnackBar != null) {
            mSnackBar!!.dismiss()
        }
    }


}