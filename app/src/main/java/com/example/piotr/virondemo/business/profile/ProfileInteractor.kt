package com.example.piotr.virondemo.business.profile

import com.example.piotr.virondemo.model.User
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Single

interface ProfileInteractor {

    val user: Observable<User>

    fun saveUser(user: User): Completable

    fun loadImage(userId: String, imagePath: String): Single<String>

    fun cleanUser()
}
