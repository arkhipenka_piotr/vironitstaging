package com.example.piotr.virondemo.resources

import android.support.annotation.StringRes

interface ResourcesManager {
    fun getString(@StringRes id: Int): String
    fun getString(@StringRes id: Int, vararg formatArgs: Any): String
}