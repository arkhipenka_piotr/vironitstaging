package com.example.piotr.virondemo.dagger.scope

import java.lang.annotation.RetentionPolicy
import javax.inject.Scope

@Retention(value = AnnotationRetention.RUNTIME)
@Scope
annotation class ActivityScope