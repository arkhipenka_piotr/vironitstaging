package com.example.piotr.virondemo.business.utils.user_validator

interface UserValidator {
    fun validateLogin(login: String?): Boolean

    fun validatePassword(password: String?): Boolean
}
