package com.example.piotr.virondemo.ui.sign_up_screen

import android.content.Intent
import android.os.Bundle
import com.arellomobile.mvp.presenter.InjectPresenter
import com.example.piotr.virondemo.R
import com.example.piotr.virondemo.ui.base.BaseActivity
import com.example.piotr.virondemo.ui.home_screen.HomeActivity
import kotlinx.android.synthetic.main.activity_sign_up.*

class SignUpActivity : BaseActivity(), SignUpView {
    override val layoutResId: Int
        get() = R.layout.activity_sign_up
    override val rootViewResId: Int
        get() = R.id.cl_signup_root

    @InjectPresenter
    lateinit var signUpPresenter: SignUpPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_sign_up)
        button_register.setOnClickListener {
            signUpPresenter.register(
                    input_login.text.toString(),
                    input_password.text.toString(),
                    input_second_password.text.toString())
        }
    }

    override fun showLoginValidError() {
        input_login.error = getString(R.string.login_valid_error)
    }

    override fun showPasswordValidError() {
        input_password.error = getString(R.string.password_valid_error)
    }

    override fun clearErrors() {
        input_layout_login.error = ""
        input_layout_password.error = ""
        input_layout_second_password.error = ""
    }

    override fun showSecondPasswordError() {
        input_layout_second_password.error = getString(R.string.passwords_must_be_the_same)
    }

    override fun onSuccesfulSignUp() {
        val intent = Intent(this, HomeActivity::class.java)
        startActivity(intent)
    }

    override fun showUnknownError() {
        input_password.error = "unknown error"
    }
}
