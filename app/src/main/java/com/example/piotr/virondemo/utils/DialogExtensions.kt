package com.example.piotr.virondemo.utils

import android.app.Activity
import android.content.Context
import android.content.DialogInterface
import android.support.v7.app.AlertDialog
import android.support.v7.view.ContextThemeWrapper
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.example.piotr.virondemo.R

fun Activity.showProgressDialog(titleText: String,
                                messageText: String?): AlertDialog? {
    if (this.isFinishing) {
        return null
    }

    val builder = AlertDialog.Builder(ContextThemeWrapper(this, R.style.AlertDialogStyle))
            .setTitle(titleText)
            .setIcon(R.mipmap.ic_launcher_round)
            .setView(messageText?.let { initProgressView(it) })
            .setCancelable(false)

    val materialDialog = builder.show()
    val window = materialDialog.window

    window.setLayout(this.resources.getDimensionPixelSize(R.dimen.progress_dialog_width),
            ViewGroup.LayoutParams.WRAP_CONTENT)
    window.setBackgroundDrawableResource(R.drawable.dialog_shape)
    materialDialog.setCanceledOnTouchOutside(false)
    materialDialog.setCancelable(false)
    return materialDialog
}

fun Context.initProgressView(messageText: String): View{
    val view = LayoutInflater.from(this).inflate(R.layout.layout_progress_dialog, null, false)
    val messageTextView = view.findViewById<TextView>(R.id.tv_message)
    if (!TextUtils.isEmpty(messageText)) {
        messageTextView.text = messageText
        messageTextView.visibility = View.VISIBLE
    } else {
        messageTextView.visibility = View.GONE
    }
    return view
}

fun Activity.showMessageDialog(title: String,
                               message: String,
                               positiveOptionsMessage: String?,
                               negativeOptionsMessage: String?,
                               positiveListener: DialogInterface.OnClickListener?,
                               negativeListener: DialogInterface.OnClickListener?,
                               cancelable: Boolean): AlertDialog? {
    if (this.isFinishing) {
        return null
    }

    //TODO
    val builder = AlertDialog.Builder(ContextThemeWrapper(this, R.style.AlertDialogStyle))
            .setTitle(title)
            .setMessage(message)
            .setPositiveButton(positiveOptionsMessage, positiveListener)
            .setNegativeButton(negativeOptionsMessage, negativeListener)
            .setCancelable(cancelable)

    val materialDialog = builder.show()
    materialDialog.setCanceledOnTouchOutside(cancelable)
    materialDialog.setCancelable(cancelable)
    return materialDialog
}