package com.example.piotr.virondemo.ui.home_screen

import android.os.Bundle
import android.support.v4.app.Fragment
import com.arellomobile.mvp.MvpAppCompatActivity
import com.arellomobile.mvp.presenter.InjectPresenter
import com.example.piotr.virondemo.App
import com.example.piotr.virondemo.R
import com.example.piotr.virondemo.ui.base.BaseFragmentActivity
import com.example.piotr.virondemo.ui.chat_screen.ChatFragment
import com.example.piotr.virondemo.ui.news_screen.NewsFragment
import com.example.piotr.virondemo.ui.profile_screen.ProfileFragment
import kotlinx.android.synthetic.main.activity_home.*

class HomeActivity : BaseFragmentActivity(), HomeView {
    override val fragmentContainerResId: Int
        get() = R.id.fragmentcontainer

    override val layoutResId: Int
        get() = R.layout.activity_home

    override val rootViewResId: Int
        get() = R.id.rl_root_home_view

    @InjectPresenter
    lateinit var presenter: HomePresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        initBottomNavigationView()
    }

    override fun createFragment(screenKey: String?, data: Any?): Fragment =
        when(screenKey) {
            NewsFragment.KEY -> NewsFragment.newInstance()
            ProfileFragment.KEY -> ProfileFragment.newInstance()
            ChatFragment.KEY -> ChatFragment.newInstance()
            else -> throw RuntimeException("No such fragment")
        }

    private fun initBottomNavigationView() {
        botton_navigation.setOnNavigationItemSelectedListener { item ->
            when (item.itemId) {
                R.id.action_chat -> {
                    presenter.moveToChatScreen()
                }
                R.id.action_news -> {
                    presenter.moveToNewsScreen()
                }
                R.id.action_profile -> {
                    presenter.moveToProfileScreen()
                }
            }
            true
        }
    }
}
