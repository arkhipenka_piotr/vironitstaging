package com.example.piotr.virondemo.dagger.modules

import android.content.Context
import android.content.SharedPreferences
import android.preference.PreferenceManager
import com.example.piotr.virondemo.business.data.user_preferences.UserPreferencesRepository
import com.example.piotr.virondemo.business.data.user_preferences.UserPreferencesRepositoryImpl
import com.f2prateek.rx.preferences2.RxSharedPreferences
import dagger.Module
import dagger.Provides
import io.reactivex.annotations.NonNull
import javax.inject.Singleton

@Module
class DataModule {

    @Provides
    @NonNull
    @Singleton
    fun provideSharedPreferences(context: Context): SharedPreferences {
        return PreferenceManager.getDefaultSharedPreferences(context)
    }

    @Provides
    @NonNull
    @Singleton
    fun provideRxSharedPreferences(sharedPreferences: SharedPreferences): RxSharedPreferences {
        return RxSharedPreferences.create(sharedPreferences)
    }

    @Provides
    @NonNull
    @Singleton
    fun provideUserPreferencesRepository(): UserPreferencesRepository {
        return UserPreferencesRepositoryImpl()
    }
}
