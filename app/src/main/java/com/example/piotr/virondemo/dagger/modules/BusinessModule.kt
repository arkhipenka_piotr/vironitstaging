package com.example.piotr.virondemo.dagger.modules

import com.example.piotr.virondemo.business.login.LoginInteractor
import com.example.piotr.virondemo.business.login.LoginInteractorImpl
import com.example.piotr.virondemo.business.profile.ProfileInteractor
import com.example.piotr.virondemo.business.profile.ProfileInteractorImpl
import com.example.piotr.virondemo.business.sign_up.SignUpInteractor
import com.example.piotr.virondemo.business.sign_up.SignUpInteractorImpl
import com.example.piotr.virondemo.business.utils.user_validator.UserValidator
import com.example.piotr.virondemo.business.utils.user_validator.UserValidatorImpl
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Singleton
@Module
class BusinessModule {

    @Provides
    @Singleton
    fun provideLoginInteractor(): LoginInteractor {
        return LoginInteractorImpl()
    }

    @Provides
    @Singleton
    internal fun provideSignUpInteractor(): SignUpInteractor {
        return SignUpInteractorImpl()
    }

    @Provides
    internal fun provideUserValidator(): UserValidator {
        return UserValidatorImpl()
    }

    @Provides
    internal fun provideProfileInteractor(): ProfileInteractor {
        return ProfileInteractorImpl()
    }
}
