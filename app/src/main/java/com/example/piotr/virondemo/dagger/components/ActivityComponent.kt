package com.example.piotr.virondemo.dagger.components

import com.example.piotr.virondemo.dagger.modules.ActivityModule
import com.example.piotr.virondemo.dagger.scope.ActivityScope
import dagger.Component

@ActivityScope
@Component(modules = [ActivityModule::class], dependencies = [AppComponent::class])
interface ActivityComponent {
}