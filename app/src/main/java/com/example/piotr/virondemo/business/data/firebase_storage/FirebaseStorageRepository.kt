package com.example.piotr.virondemo.business.data.firebase_storage

import io.reactivex.Single

interface FirebaseStorageRepository {
    fun loadImage(userId: String, byteArray: ByteArray): Single<String>
}