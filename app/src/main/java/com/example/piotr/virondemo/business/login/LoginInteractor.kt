package com.example.piotr.virondemo.business.login

import com.example.piotr.virondemo.model.User
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.auth.AuthResult
import com.google.firebase.auth.UserInfo

import io.reactivex.Completable
import io.reactivex.Single

interface LoginInteractor {

    val isUserEntered: Boolean?

    fun validateLogin(login: String): Boolean

    fun validatePassword(password: String): Boolean

    fun login(email: String, password: String): Completable

    fun loginAnon(): Completable

    fun saveUser(user: User)
}
