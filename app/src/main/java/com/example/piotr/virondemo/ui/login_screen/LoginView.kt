package com.example.piotr.virondemo.ui.login_screen

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy
import com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import com.example.piotr.virondemo.ui.base.BaseView
import com.example.piotr.virondemo.utils.AddToEndSingleByTagStateStrategy

interface LoginView : BaseView {
    @StateStrategyType(AddToEndSingleStrategy::class)
    fun showLoginValidError()

    @StateStrategyType(AddToEndSingleStrategy::class)
    fun showPasswordValidError()

    @StateStrategyType(AddToEndSingleStrategy::class)
    fun clearErrors()

    @StateStrategyType(AddToEndSingleStrategy::class)
    fun showUnknownError()

    @StateStrategyType(OneExecutionStateStrategy::class)
    fun onSuccesfulLogIn()
}
