package com.example.piotr.virondemo.ui.home_screen

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.example.piotr.virondemo.App
import com.example.piotr.virondemo.business.profile.ProfileInteractor
import com.example.piotr.virondemo.ui.chat_screen.ChatFragment
import com.example.piotr.virondemo.ui.news_screen.NewsFragment
import com.example.piotr.virondemo.ui.profile_screen.ProfileFragment
import javax.inject.Inject

@InjectViewState
class HomePresenter : MvpPresenter<HomeView>() {
    @Inject
    lateinit var profileInteractor: ProfileInteractor

    init {
        App.getAppComponent()
                .inject(this)
    }

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()

        moveToNewsScreen()
    }

    fun moveToNewsScreen() {
        App.instance.getRouter().navigateTo(NewsFragment.KEY)
    }

    fun moveToProfileScreen() {
        App.instance.getRouter().navigateTo(ProfileFragment.KEY)
    }

    fun moveToChatScreen() {
        App.instance.getRouter().navigateTo(ChatFragment.KEY)
    }
}
