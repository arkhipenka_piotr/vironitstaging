package com.example.piotr.virondemo.business.profile

import android.content.Context
import com.example.piotr.virondemo.App
import com.example.piotr.virondemo.business.data.firebase_database.FirebaseDatabaseRepository
import com.example.piotr.virondemo.business.data.firebase_storage.FirebaseStorageRepository
import com.example.piotr.virondemo.business.data.user_preferences.UserPreferencesRepository
import com.example.piotr.virondemo.model.User
import com.google.android.gms.common.util.IOUtils
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Single
import java.io.File
import java.io.IOException
import javax.inject.Inject

class ProfileInteractorImpl : ProfileInteractor {
    @Inject
    lateinit var firebaseDatabaseRepository: FirebaseDatabaseRepository

    @Inject
    lateinit var userPreferencesRepository: UserPreferencesRepository

    @Inject
    lateinit var firebaseStorageRepository: FirebaseStorageRepository

    @Inject
    lateinit var context: Context

    override val user: Observable<User>
        get() {
            val id = userPreferencesRepository.getUserId()
            return firebaseDatabaseRepository.getUserObservable(id!!)
        }

    init {
        App.getAppComponent()
                .inject(this)
    }

    override fun saveUser(user: User): Completable {
        return firebaseDatabaseRepository.loadUser(user)
    }

    override fun loadImage(userId: String, imagePath: String): Single<String> {
        return try {
            val bytes = IOUtils.toByteArray(File(imagePath))
            firebaseStorageRepository.loadImage(userId, bytes)
        } catch (e: IOException) {
            Single.error(e)
        }

    }

    override fun cleanUser() {
        userPreferencesRepository.setUserId(null)
    }
}
