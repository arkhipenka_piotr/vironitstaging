package com.example.piotr.virondemo.ui.base

import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.support.annotation.IdRes
import android.support.annotation.LayoutRes
import android.support.design.widget.BaseTransientBottomBar
import android.support.design.widget.Snackbar
import android.support.v4.app.Fragment
import android.support.v7.app.AlertDialog
import android.view.View
import com.arellomobile.mvp.MvpAppCompatActivity

import com.example.piotr.virondemo.R
import com.example.piotr.virondemo.utils.*
import hugo.weaving.DebugLog
import ru.terrakok.cicerone.Navigator
import ru.terrakok.cicerone.android.SupportFragmentNavigator


abstract class BaseActivity : MvpAppCompatActivity(), BaseView {

    private var mDialog: AlertDialog? = null

    private var mSnackBar: Snackbar? = null

    protected val mHandler = Handler()

    @get:LayoutRes
    abstract val layoutResId: Int

    @get:IdRes
    abstract val rootViewResId: Int

    @DebugLog
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(layoutResId)
        initFromIntentExtras(savedInstanceState)
    }

    override fun showProgress() {
        showProgress(null)
    }

    override fun cancelScreen() {

    }

    @DebugLog
    open fun initFromIntentExtras(args: Bundle?) {
        AppLog.logActivity(this)
    }

    @DebugLog
    open fun initBeforeLayoutAttachment() {
        AppLog.logActivity(this)
    }

    @DebugLog
    override fun setContentView(layoutResID: Int) {
        initBeforeLayoutAttachment()
        super.setContentView(layoutResID)
    }

    override fun showAutocloseableMessage(message: String) {
        showMessage(message, true, null, null)
    }

    override fun showDialogMessage(message: String, closable: Boolean) {
        val title = resources.getString(R.string.app_name)
        showDialogWithOptions(title, message, null, null, null, null, closable)

    }

    override fun showDialogWithOptions(title: String,
                              message: String,
                              positiveOptionMessage: String?,
                              negativeOptionMessage: String?,
                              positiveListener: DialogInterface.OnClickListener?,
                              negativeListener: DialogInterface.OnClickListener?,
                              cancelable: Boolean) {
        hideDialogMessage()
        mDialog = showMessageDialog(title, message, positiveOptionMessage,
                negativeOptionMessage, positiveListener, negativeListener, cancelable)

    }

    override fun showMessage(message: String, closable: Boolean,
                             actionMessage: String?,
                             actionListener: View.OnClickListener?) {
        hideMessage()
        var rootView: View? = findViewById(rootViewResId)
        val duration = if (closable) BaseTransientBottomBar.LENGTH_SHORT else BaseTransientBottomBar.LENGTH_INDEFINITE
        if (rootView == null) {
            rootView = window.decorView
        }
        mSnackBar = showSnackBar(rootView, message, actionMessage, actionListener, duration)

    }

    override fun hideDialogMessage() {
        if (mDialog != null && mDialog!!.isShowing) {
            mDialog!!.dismiss()
        }

    }

    override fun hideMessage() {
        if (mSnackBar != null) {
            mSnackBar!!.dismiss()
        }

    }

    override fun onPause() {
        hideKeyboard()
        super.onPause()
    }

    @DebugLog
    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
    }

    override fun showProgress(messageText: String?) {
        hideProgress()
        val titleText = resources.getString(R.string.progress_title)
        mDialog = showProgressDialog(titleText, messageText)

    }

    override fun hideProgress() {
        if (mDialog != null && mDialog!!.isShowing) {
            mDialog!!.dismiss()
        }

    }
}
