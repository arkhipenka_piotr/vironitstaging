package com.example.piotr.virondemo

import android.app.Activity
import android.app.Application
import android.app.Service
import android.content.BroadcastReceiver
import com.example.piotr.virondemo.dagger.components.AppComponent
import com.example.piotr.virondemo.dagger.components.DaggerAppComponent
import com.example.piotr.virondemo.dagger.modules.ContextModule

import com.facebook.FacebookSdk
import com.facebook.appevents.AppEventsLogger
import com.squareup.leakcanary.LeakCanary
import com.twitter.sdk.android.core.TwitterAuthConfig
import com.twitter.sdk.android.core.TwitterCore
import dagger.android.*

import io.fabric.sdk.android.Fabric
import ru.terrakok.cicerone.Cicerone
import ru.terrakok.cicerone.Router
import javax.inject.Inject
import ru.terrakok.cicerone.NavigatorHolder



class App : Application(), HasActivityInjector, HasServiceInjector, HasBroadcastReceiverInjector {

    private lateinit var cicerone: Cicerone<Router>

    companion object {
        const val COMPUTATION_SCHEDULER = "computation_scheduler"
        const val UI_SCHEDULER = "ui_scheduler"
        const val IO_SCHEDULER = "io_scheduler"

        private lateinit var appComponent: AppComponent
        lateinit var instance: App

        fun getAppComponent(): AppComponent {
            return appComponent
        }
    }

    override fun onCreate() {
        super.onCreate()

        instance = this
        FacebookSdk.sdkInitialize(applicationContext)
        AppEventsLogger.activateApp(this)

        val authConfig = TwitterAuthConfig(getString(R.string.twitter_consumer_key),
                getString(R.string.twitter_consumer_secret))
        Fabric.with(this, TwitterCore(authConfig))

        initDagger2()
        initLeakCanary()
        initCicerone()

    }

    private fun initCicerone() {
        cicerone = Cicerone.create()
    }

    private fun initLeakCanary() {
        if (LeakCanary.isInAnalyzerProcess(this)) {
            return
        }
        LeakCanary.install(this)
    }
    override fun activityInjector(): AndroidInjector<Activity>? {
        return activityDispatchingAndroidInjector
    }

    override fun broadcastReceiverInjector(): AndroidInjector<BroadcastReceiver>? {
        return broadcastReceiverDispatchingAndroidInjector
    }

    override fun serviceInjector(): AndroidInjector<Service>? {
        return serviceDispatchingAndroidInjector
    }

    @Inject
    lateinit var activityDispatchingAndroidInjector: DispatchingAndroidInjector<Activity>

    @Inject
    lateinit var broadcastReceiverDispatchingAndroidInjector: DispatchingAndroidInjector<BroadcastReceiver>

    @Inject
    lateinit var serviceDispatchingAndroidInjector: DispatchingAndroidInjector<Service>

    private fun initDagger2() {
        appComponent = DaggerAppComponent.builder()
                .appContext(this)
                .contextModule(ContextModule(applicationContext))
                .build()

        appComponent.inject(this)
    }

    fun getNavigatorHolder(): NavigatorHolder {
        return cicerone.navigatorHolder
    }

    fun getRouter(): Router {
        return cicerone.router
    }
}
