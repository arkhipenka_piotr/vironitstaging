package com.example.piotr.virondemo.ui.base

import android.app.Activity
import android.content.Intent

import com.arellomobile.mvp.MvpPresenter
import com.example.piotr.virondemo.App
import com.example.piotr.virondemo.utils.AppLog
import io.reactivex.Scheduler

import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import javax.inject.Inject
import javax.inject.Named

abstract class BasePresenter<View : BaseView> : MvpPresenter<View>() {

    private val mLiteCompositeDisposable = CompositeDisposable()
    private val mHardCompositeDisposable = CompositeDisposable()

    override fun attachView(view: View) {
        AppLog.logPresenter(this)
        super.attachView(view)
    }

    override fun onFirstViewAttach() {
        AppLog.logPresenter(this)
        super.onFirstViewAttach()
    }

    override fun detachView(view: View) {
        clearLiteDisposable()
        AppLog.logPresenter(this)
        super.detachView(view)
    }

    override fun destroyView(view: View) {

        AppLog.logPresenter(this)
        super.destroyView(view)
    }

    override fun onDestroy() {
        clearHardDisposable()
        AppLog.logPresenter(this)
        super.onDestroy()
    }

    fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent, activity: Activity) {
        AppLog.logPresenter(this)

    }

    open fun onRequestPermissionsResult(
            requestCode: Int,
            permissions: Array<String>,
            grantResults: IntArray,
            activity: Activity
    ) {
        AppLog.logPresenter(this)

    }

    protected fun addLiteDisposable(disposable: Disposable?) {
        if (disposable != null) {
            mLiteCompositeDisposable.add(disposable)
        }
    }

    protected fun addHardDisposable(disposable: Disposable?) {
        if (disposable != null) {
            mHardCompositeDisposable.add(disposable)
        }
    }

    open protected fun clearHardDisposable() {
        mHardCompositeDisposable.clear()
    }

    open protected fun clearLiteDisposable() {
        mHardCompositeDisposable.clear()
    }

    //TODO
    protected fun getString() {

    }

    protected fun clearDisposableIfPossible(disposable: Disposable?) {
        if (disposable != null && !disposable.isDisposed) {
            disposable.dispose()
        }
    }
}
