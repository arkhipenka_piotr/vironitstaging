package com.example.piotr.virondemo.business.data.firebase_database

import com.example.piotr.virondemo.App
import com.example.piotr.virondemo.model.User
import com.google.firebase.database.FirebaseDatabase
import durdinapps.rxfirebase2.RxFirebaseDatabase
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Single
import javax.inject.Inject

class FirebaseDatabaseRepositoryImpl : FirebaseDatabaseRepository {

    @Inject
    lateinit var firebaseDatabase: FirebaseDatabase

    companion object {
        private const val USERS_REFERENCE = "users"
    }

    init {
        App.getAppComponent()
                .inject(this)
    }
    override fun getUserById(id: String): Single<User> {
        val userReference = firebaseDatabase
                .reference
                .child(USERS_REFERENCE)
                .child(id)

        return RxFirebaseDatabase.observeSingleValueEvent(userReference, User::class.java)
                .toSingle()
    }

    override fun loadUser(user: User): Completable {
        if (user.id != null) {
            val userReference = firebaseDatabase
                    .reference
                    .child(USERS_REFERENCE)
                    .child(user.id)

            return RxFirebaseDatabase.setValue(userReference, user)
        }
        return Completable.complete()
    }

    override fun getUserObservable(id: String): Observable<User> {
        val userReference = firebaseDatabase
                .reference
                .child(USERS_REFERENCE)
                .child(id)

        return RxFirebaseDatabase.observeValueEvent(userReference, User::class.java)
                .toObservable()
    }
}
