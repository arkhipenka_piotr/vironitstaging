package com.example.piotr.virondemo.ui.splash_screen

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.example.piotr.virondemo.App
import java.util.*

@InjectViewState
class SplashPresenter : MvpPresenter<SplashView>() {

    companion object {
        private const val THREE_SECOND = 3000L
    }

    init {
        App.getAppComponent()
                .inject(this)
    }

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()

        val timer = Timer()
        timer.schedule(object : TimerTask() {
            override fun run() {
                viewState.onTimerFinished()
            }
        }, THREE_SECOND)
    }
}
