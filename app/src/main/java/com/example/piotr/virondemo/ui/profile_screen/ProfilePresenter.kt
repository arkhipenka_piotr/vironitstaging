package com.example.piotr.virondemo.ui.profile_screen

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.example.piotr.virondemo.App
import com.example.piotr.virondemo.business.profile.ProfileInteractor
import com.example.piotr.virondemo.model.User
import com.example.piotr.virondemo.ui.base.BaseAppPresenter
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import java.io.File
import javax.inject.Inject

@InjectViewState
class ProfilePresenter : BaseAppPresenter<ProfileView>() {

    @Inject
    lateinit var profileInteractor: ProfileInteractor

    private var user: User? = null
    private var newImagePath: String? = null

    init {
        App.getAppComponent()
                .inject(this)
    }

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()

        profileInteractor.user
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::saveAndBindUser, this::showError)
    }

    fun saveUserAndExit(firstName: String?, lastName: String?, email: String?) {

        user?.firstName = firstName
        user?.lastName = lastName
        user?.email = email

        if (user != null) {
            viewState.showProgress()
            if ((newImagePath == null || newImagePath == user!!.photoUrl)) {
                val disposable = profileInteractor.saveUser(user!!)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .doFinally(viewState::hideProgress)
                        .subscribe(this::exit, this::showError)
                addHardDisposable(disposable)

            } else {
                val disposable = profileInteractor.loadImage(user?.id!!, newImagePath!!)
                        .map<User> { imagepath ->
                            user!!.photoUrl = imagepath
                            user
                        }
                        .flatMapCompletable(profileInteractor::saveUser)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .doFinally(viewState::hideProgress)
                        .subscribe(viewState::exit, this::showError)
                addHardDisposable(disposable)
            }
        }
    }

    private fun saveAndBindUser(user: User) {
        this.user = user
        newImagePath = user.photoUrl
        viewState.bindUserData(user)
        if (user.photoUrl != null) {
            viewState.updateImage(user.photoUrl)
        }
    }

    private fun showError(throwable: Throwable) {
        viewState.showAutocloseableMessage(throwable.message?:"")
    }

    private fun exit() {
        viewState.exit()
    }

    fun updateImageByPath(path: String) {
        newImagePath = path
        viewState.updateImage(File(path))
    }

    fun exitFromAccount() {
        profileInteractor.cleanUser()
        viewState.moveToSignIn()
    }
}
