package com.example.piotr.virondemo.ui.splash_screen

import android.content.Intent

import com.arellomobile.mvp.MvpAppCompatActivity
import com.arellomobile.mvp.presenter.InjectPresenter
import com.example.piotr.virondemo.ui.login_screen.LoginActivity

class SplashActivity : MvpAppCompatActivity(), SplashView {

    @InjectPresenter
    lateinit var splashPresenter: SplashPresenter

    override fun onTimerFinished() {
        val intent = Intent(this, LoginActivity::class.java)

        startActivity(intent)
    }
}
