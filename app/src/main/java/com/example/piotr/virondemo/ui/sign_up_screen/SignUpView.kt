package com.example.piotr.virondemo.ui.sign_up_screen

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy
import com.arellomobile.mvp.viewstate.strategy.SkipStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import com.example.piotr.virondemo.ui.base.BaseView
import com.example.piotr.virondemo.utils.AddToEndSingleByTagStateStrategy

interface SignUpView : BaseView {
    @StateStrategyType(AddToEndSingleStrategy::class)
    fun showLoginValidError()

    @StateStrategyType(AddToEndSingleStrategy::class)
    fun showPasswordValidError()

    @StateStrategyType(AddToEndSingleStrategy::class)
    fun clearErrors()

    @StateStrategyType(AddToEndSingleStrategy::class)
    fun showSecondPasswordError()

    @StateStrategyType(SkipStrategy::class)
    fun onSuccesfulSignUp()

    @StateStrategyType(AddToEndSingleStrategy::class)
    fun showUnknownError()
}
