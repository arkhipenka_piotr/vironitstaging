package com.example.piotr.virondemo.dagger.modules

import com.example.piotr.virondemo.App
import dagger.Module
import dagger.Provides
import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Named
import javax.inject.Singleton

@Module
class SchedulersModule {

    @Singleton
    @Named(App.COMPUTATION_SCHEDULER)
    @Provides
    fun provideComputationScheduler() = Schedulers.computation()

    @Singleton
    @Named(App.IO_SCHEDULER)
    @Provides
    fun provideIOScheduler() = Schedulers.io()

    @Singleton
    @Named(App.UI_SCHEDULER)
    @Provides
    fun provideUIScheduler() = AndroidSchedulers.mainThread()
}