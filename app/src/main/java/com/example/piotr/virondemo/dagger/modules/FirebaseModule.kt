package com.example.piotr.virondemo.dagger.modules

import com.example.piotr.virondemo.business.data.firebase_database.FirebaseDatabaseRepository
import com.example.piotr.virondemo.business.data.firebase_database.FirebaseDatabaseRepositoryImpl
import com.example.piotr.virondemo.business.data.firebase_storage.FirebaseStorageRepository
import com.example.piotr.virondemo.business.data.firebase_storage.FirebaseStorageRepositoryImpl
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.storage.FirebaseStorage
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class FirebaseModule {

    @Provides
    @Singleton
    fun provideFirebaseAuth(): FirebaseAuth {
        return FirebaseAuth.getInstance()
    }

    @Provides
    @Singleton
    fun provideFirebaseDatabase(): FirebaseDatabase {
        return FirebaseDatabase.getInstance()
    }

    @Provides
    @Singleton
    fun provideFirebaseDatabaseRepository(): FirebaseDatabaseRepository {
        return FirebaseDatabaseRepositoryImpl()
    }

    @Provides
    @Singleton
    fun provideFirebaseStorage(): FirebaseStorage {
        return FirebaseStorage.getInstance()
    }

    @Provides
    fun provideFirebaseStorageRepository(): FirebaseStorageRepository {
        return FirebaseStorageRepositoryImpl()
    }
}
