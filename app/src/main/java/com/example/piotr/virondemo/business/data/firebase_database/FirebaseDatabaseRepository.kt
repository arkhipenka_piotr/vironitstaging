package com.example.piotr.virondemo.business.data.firebase_database

import com.example.piotr.virondemo.model.User

import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Single

interface FirebaseDatabaseRepository {

    fun getUserById(id: String): Single<User>
    fun getUserObservable(id: String): Observable<User>
    fun loadUser(user: User): Completable
}
