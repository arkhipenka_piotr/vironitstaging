package com.example.piotr.virondemo.ui.base

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy
import com.arellomobile.mvp.viewstate.strategy.SkipStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType

@StateStrategyType(SkipStrategy::class)
interface BaseView : MvpView, ProgressView, MessageView {

    @StateStrategyType(AddToEndSingleStrategy::class)
    fun cancelScreen()
}