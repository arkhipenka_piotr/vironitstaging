package com.example.piotr.virondemo.business.login

import com.example.piotr.virondemo.App
import com.example.piotr.virondemo.business.data.firebase_database.FirebaseDatabaseRepository
import com.example.piotr.virondemo.business.data.user_preferences.UserPreferencesRepository
import com.example.piotr.virondemo.business.utils.user_validator.UserValidator
import com.example.piotr.virondemo.model.User
import com.google.firebase.auth.AuthResult
import com.google.firebase.auth.FirebaseAuth
import durdinapps.rxfirebase2.RxFirebaseAuth
import io.reactivex.Completable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class LoginInteractorImpl : LoginInteractor {

    @Inject
    lateinit var userValidator: UserValidator

    @Inject
    lateinit var firebaseAuth: FirebaseAuth

    @Inject
    lateinit var userPreferencesRepository: UserPreferencesRepository

    @Inject
    lateinit var firebaseDatabaseRepository: FirebaseDatabaseRepository

    private var saveDisposable: Disposable? = null

    override val isUserEntered: Boolean?
        get() = userPreferencesRepository.getUserId() != null

    init {
        App.getAppComponent()
                .inject(this)
    }

    override fun validateLogin(login: String): Boolean {
        return userValidator.validateLogin(login)
    }

    override fun validatePassword(password: String): Boolean {
        return userValidator.validatePassword(password)
    }

    override fun login(email: String, password: String): Completable {
        return RxFirebaseAuth.signInWithEmailAndPassword(firebaseAuth, email, password)
                .doOnSuccess(this::saveUserId)
                .ignoreElement()
    }

    override fun loginAnon(): Completable {
        return RxFirebaseAuth.signInAnonymously(firebaseAuth)
                .ignoreElement()
    }

    private fun saveUserId(authResult: AuthResult) {
        userPreferencesRepository.setUserId(authResult.user.uid)
    }

    override fun saveUser(user: User) {
        if (user.id != null) {
            userPreferencesRepository.setUserId(user.id)
            saveDisposable = firebaseDatabaseRepository.getUserById(user.id)
                    .subscribeOn(Schedulers.io())
                    .observeOn(Schedulers.io())
                    .subscribe({this.onSuccesLoading()}, {this.onErrorGetting(user)})
        }
    }

    private fun onSuccesLoading() {
        saveDisposable?.dispose()
    }

    private fun onErrorLoading(throwable: Throwable) {
        userPreferencesRepository.setUserId(null)
        saveDisposable?.dispose()
    }

    private fun onErrorGetting(user: User) {
        saveDisposable = firebaseDatabaseRepository.loadUser(user)
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.io())
                .subscribe(this::onSuccesLoading, this::onErrorLoading)
    }
}
