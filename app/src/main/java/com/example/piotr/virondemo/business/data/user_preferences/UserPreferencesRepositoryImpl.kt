package com.example.piotr.virondemo.business.data.user_preferences

import android.content.SharedPreferences
import androidx.core.content.edit
import com.example.piotr.virondemo.App
import javax.inject.Inject

class UserPreferencesRepositoryImpl : UserPreferencesRepository {
    @Inject
    lateinit var sharedPreferences: SharedPreferences

    init {
        App.getAppComponent()
                .inject(this)
    }
    companion object {
        const val USER_ID_KEY = "user_id_key"
    }

    override fun getUserId(): String? {
        return sharedPreferences.getString(USER_ID_KEY, null)
    }

    override fun setUserId(id: String?) {
        sharedPreferences.edit {
            putString(USER_ID_KEY, id)
        }
    }


}