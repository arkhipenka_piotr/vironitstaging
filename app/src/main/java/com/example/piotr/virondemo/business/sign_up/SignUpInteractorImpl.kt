package com.example.piotr.virondemo.business.sign_up

import com.example.piotr.virondemo.App
import com.example.piotr.virondemo.business.data.firebase_database.FirebaseDatabaseRepository
import com.example.piotr.virondemo.business.data.user_preferences.UserPreferencesRepository
import com.example.piotr.virondemo.business.utils.user_validator.UserValidator
import com.example.piotr.virondemo.model.User
import com.google.firebase.auth.FirebaseAuth
import durdinapps.rxfirebase2.RxFirebaseAuth
import io.reactivex.Completable
import javax.inject.Inject

class SignUpInteractorImpl : SignUpInteractor {
    @Inject
    lateinit var userValidator: UserValidator

    @Inject
    lateinit var userPreferencesRepository: UserPreferencesRepository

    @Inject
    lateinit var firebaseAuth: FirebaseAuth

    @Inject
    lateinit var firebaseDatabaseRepository: FirebaseDatabaseRepository

    init {
        App.getAppComponent()
                .inject(this)
    }

    override fun validateLogin(login: String): Boolean {
        return userValidator.validateLogin(login)
    }

    override fun validatePassword(password: String): Boolean {
        return userValidator.validatePassword(password)
    }

    override fun signUp(email: String, password: String): Completable {
        return RxFirebaseAuth.createUserWithEmailAndPassword(firebaseAuth, email, password)
                .map { userInfo ->
                    User(userInfo.user.uid,
                            userInfo.user.displayName, null,
                            userInfo.user.email,
                            if (userInfo.user.photoUrl == null) null else userInfo.user.photoUrl!!.path)
                }
                .flatMapCompletable(this::saveUser)
    }

    private fun saveUser(user: User): Completable? {
        userPreferencesRepository.setUserId(user.id)
        return firebaseDatabaseRepository.loadUser(user)
    }
}
