package com.example.piotr.virondemo.resources

import android.content.Context
import com.example.piotr.virondemo.App
import javax.inject.Inject

class ResourcesManagerImpl : ResourcesManager{
    @Inject
    lateinit var context: Context

    init {
        App.getAppComponent()
                .inject(this)
    }

    override fun getString(id: Int): String {
        var str: String? = null
        try {
            str = context.getString(id)
        } catch (e: Exception) {
        }

        return if (str != null) str else ""
    }

    override fun getString(id: Int, vararg formatArgs: Any): String {
        var str: String? = null
        try {
            str = context.getString(id, *formatArgs)
        } catch (e: Exception) {
        }


        return if (str != null) str else ""
    }
}