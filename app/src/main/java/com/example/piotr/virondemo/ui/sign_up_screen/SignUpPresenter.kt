package com.example.piotr.virondemo.ui.sign_up_screen

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.example.piotr.virondemo.App
import com.example.piotr.virondemo.business.sign_up.SignUpInteractor
import com.example.piotr.virondemo.ui.base.BaseAppPresenter
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

@InjectViewState
class SignUpPresenter : BaseAppPresenter<SignUpView>() {
    @Inject
    lateinit var signUpInteractor: SignUpInteractor

    var signUpDisposable: Disposable? = null

    init {
        App.getAppComponent()
                .inject(this)
    }
    internal fun register(email: String, password: String, secondPassword: String) {
        viewState.showProgress()
        if (signUpInteractor.validateLogin(email) && signUpInteractor.validatePassword(password) && password == secondPassword) {
            signUpDisposable = signUpInteractor.signUp(email, password)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .doFinally(viewState::hideProgress)
                    .subscribe(this::onSuccesfulSignUp, this::onError)
        } else {
            viewState.hideProgress()
            if (!signUpInteractor.validateLogin(email)) {
                viewState.showLoginValidError()
            }
            if (!signUpInteractor.validatePassword(password)) {
                viewState.showPasswordValidError()
            }
            if (password != secondPassword) {
                viewState.showSecondPasswordError()
            }
        }
    }

    private fun onSuccesfulSignUp() {
        signUpDisposable?.dispose()
        viewState.onSuccesfulSignUp()
    }

    private fun onError(throwable: Throwable) {
        signUpDisposable?.dispose()
        viewState.showAutocloseableMessage(throwable.message?:"null")
    }
}
