package com.example.piotr.virondemo.ui.base

import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import com.example.piotr.virondemo.utils.AddToEndSingleByTagStateStrategy

interface ProgressView {

    @StateStrategyType(AddToEndSingleByTagStateStrategy::class, tag = "progress")
    fun showProgress()

    @StateStrategyType(AddToEndSingleByTagStateStrategy::class, tag = "progress")
    fun showProgress(messageText: String?)

    @StateStrategyType(AddToEndSingleByTagStateStrategy::class, tag = "progress")
    fun hideProgress()
}