package com.example.piotr.virondemo.ui.base

import android.support.annotation.IdRes
import android.support.v4.app.Fragment
import com.example.piotr.virondemo.App
import ru.terrakok.cicerone.Navigator
import ru.terrakok.cicerone.android.SupportFragmentNavigator

abstract class BaseFragmentActivity : BaseActivity() {
    private lateinit var navigator: Navigator

    @get:IdRes
    abstract val fragmentContainerResId: Int

    init {
        initNavigator()
    }

    private fun initNavigator() {
        navigator = object : SupportFragmentNavigator(supportFragmentManager, fragmentContainerResId) {
            override fun exit() {
                finish()
            }

            override fun createFragment(screenKey: String?, data: Any?): Fragment {
                return this@BaseFragmentActivity.createFragment(screenKey, data)
            }

            override fun showSystemMessage(message: String) {
                showAutocloseableMessage(message)
            }

        }
    }

    abstract fun createFragment(screenKey: String?, data: Any?): Fragment

    override fun onResume() {
        super.onResume()
        App.instance.getNavigatorHolder().setNavigator(navigator);
    }

    override fun onPause() {
        super.onPause()
        App.instance.getNavigatorHolder().removeNavigator()
    }
}