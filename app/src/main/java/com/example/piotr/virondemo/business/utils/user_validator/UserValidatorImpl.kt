package com.example.piotr.virondemo.business.utils.user_validator

class UserValidatorImpl : UserValidator {

    override fun validateLogin(login: String?): Boolean {
        //        return (login != null) && login.matches("\\b[A-Z0-9._%-]+@[A-Z0-9.-]+\\.[A-Z]{2,4}\\b");
        return true
    }

    override fun validatePassword(password: String?): Boolean {
        return password != null && password.matches("[A-Za-z0-9_]+".toRegex())
    }
}
