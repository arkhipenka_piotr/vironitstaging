package com.example.piotr.virondemo.business.data.user_preferences

interface UserPreferencesRepository {
    fun getUserId(): String?
    fun setUserId(id: String?)
}