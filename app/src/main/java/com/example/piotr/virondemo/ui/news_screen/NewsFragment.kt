package com.example.piotr.virondemo.ui.news_screen

import com.example.piotr.virondemo.R
import com.example.piotr.virondemo.ui.base.BaseFragment

class NewsFragment : BaseFragment(), NewsView {

    companion object {
        const val KEY = "NewsFragment"
        fun newInstance() = NewsFragment()
    }
    override val layoutResId: Int
        get() = R.layout.fragment_news
}