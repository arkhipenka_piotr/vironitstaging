package com.example.piotr.virondemo.business.utils

import com.f2prateek.rx.preferences2.Preference
import java.io.*

class SimpleConverter<T : Serializable> : Preference.Converter<T> {

    override fun deserialize(serialized: String): T {
        val bais = ByteArrayInputStream(serialized.toByteArray(charset("ISO-8859-1")))
        val ois = ObjectInputStream(bais)

        return ois.readObject() as T
    }

    override fun serialize(value: T): String {
        val baos = ByteArrayOutputStream()
        val oos = ObjectOutputStream(baos)
        oos.writeObject(value)
        oos.close()

        return baos.toString("ISO-8859-1")
    }
}