package com.example.piotr.virondemo.ui.chat_screen

import com.example.piotr.virondemo.R
import com.example.piotr.virondemo.ui.base.BaseFragment

class ChatFragment : BaseFragment(), ChatView {

    companion object {
        const val KEY = "ChatFragment"
        fun newInstance() = ChatFragment()
    }

    override val layoutResId: Int
        get() = R.layout.fragment_chat
}