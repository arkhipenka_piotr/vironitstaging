package com.example.piotr.virondemo.ui.login_screen

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.example.piotr.virondemo.App
import com.example.piotr.virondemo.business.login.LoginInteractor
import com.example.piotr.virondemo.model.User
import com.example.piotr.virondemo.ui.base.BaseAppPresenter
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

@InjectViewState
class LoginPresenter : BaseAppPresenter<LoginView>() {
    @Inject
    lateinit var loginInteractor: LoginInteractor

    init {
        App.getAppComponent()
                .inject(this)
    }

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()

        if (loginInteractor.isUserEntered!!) {
            viewState.onSuccesfulLogIn()
        }
    }

    internal fun logIn(login: String, password: String) {
        viewState.clearErrors()
        viewState.showProgress("EEEE BOY")
        if (!loginInteractor.validatePassword(password)) {
            viewState.hideProgress()
            viewState.showPasswordValidError()
        }
        if (!loginInteractor.validateLogin(login)) {
            viewState.hideProgress()
            viewState.showLoginValidError()
        }
        if (loginInteractor.validatePassword(password) && loginInteractor.validateLogin(login)) {
            val disposable = loginInteractor.login(login, password)
                    .subscribeOn(Schedulers.io())
                    .doFinally(viewState::hideProgress)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(this::onSuccesfulLogin, this::onError)
            addHardDisposable(disposable)
        }
    }

    internal fun saveUserAndGo(id: String?, firstName: String?, lastName: String?, email: String?, photoUrl: String?) {
        val user = User(id, firstName, lastName, email, photoUrl)
        loginInteractor.saveUser(user)

        val disposable = loginInteractor.loginAnon()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(viewState::onSuccesfulLogIn, this::onError)
        addHardDisposable(disposable)
    }

    private fun onSuccesfulLogin() {
        viewState.onSuccesfulLogIn()
    }

    private fun onError(throwable: Throwable) {
        viewState.showAutocloseableMessage(throwable.message?:"")
    }
}
