package com.example.piotr.virondemo.ui.login_screen

import android.content.Intent
import android.os.Bundle
import com.arellomobile.mvp.presenter.InjectPresenter
import com.example.piotr.virondemo.R
import com.example.piotr.virondemo.ui.base.BaseActivity
import com.example.piotr.virondemo.ui.home_screen.HomeActivity
import com.example.piotr.virondemo.ui.sign_up_screen.SignUpActivity
import com.facebook.CallbackManager
import com.facebook.FacebookCallback
import com.facebook.FacebookException
import com.facebook.Profile
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.tasks.Task
import com.twitter.sdk.android.core.*
import com.twitter.sdk.android.core.models.User
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : BaseActivity(), LoginView {

    override val layoutResId: Int
        get() = R.layout.activity_login
    override val rootViewResId: Int
        get() = R.id.cl_login_root

    @InjectPresenter
    lateinit var loginPresenter: LoginPresenter

    private lateinit var fbCallbackManager: CallbackManager
    private lateinit var googleSignInClient: GoogleSignInClient


    companion object {
        private const val RC_SIGN_IN = 9001
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        initFBLoginButton()
        initGoogleLoginButton()
        initTwitterLoginButton()

        button_enter.setOnClickListener {
            loginPresenter.logIn(input_login.text.toString(), input_password.text.toString())
        }

        text_register.setOnClickListener {
            val intent = Intent(this, SignUpActivity::class.java)
            startActivity(intent)
        }
    }

    private fun initTwitterLoginButton() {
        bt_fake_twitter.setOnClickListener { twitter_sign_in_button.performClick() }
        twitter_sign_in_button.callback = object : Callback<TwitterSession>() {
            override fun success(result: Result<TwitterSession>) {
                val twitterApiClient = TwitterCore.getInstance().apiClient
                val service = twitterApiClient.accountService

                service.verifyCredentials(false, false, object : Callback<User>() {
                    override fun success(result: Result<User>) {
                        loginPresenter.saveUserAndGo(result.data.idStr.toString(),
                                result.data.name,
                                null,
                                result.data.email,
                                result.data.profileImageUrlHttps.replace("_normal", ""))
                    }

                    override fun failure(e: TwitterException) {

                    }
                })
            }

            override fun failure(e: TwitterException) {
                input_layout_password.error = e.message
            }
        }
    }

    private fun initGoogleLoginButton() {

        val googleSignInOptions = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build()

        googleSignInClient = GoogleSignIn.getClient(this, googleSignInOptions)

        google_sign_in_button.setOnClickListener { signInWithGoogle() }
    }

    private fun signInWithGoogle() {
        val signInIntent = googleSignInClient.signInIntent
        startActivityForResult(signInIntent, RC_SIGN_IN)
    }

    private fun initFBLoginButton() {
        bt_fake_facebook.setOnClickListener { facebook_sign_in_button
                .performClick()}
        LoginManager.getInstance().logOut()
        fbCallbackManager = CallbackManager.Factory.create()
        facebook_sign_in_button!!.registerCallback(fbCallbackManager, object : FacebookCallback<LoginResult> {
            override fun onSuccess(loginResult: LoginResult) {
                val profile = Profile.getCurrentProfile()
                loginPresenter.saveUserAndGo(profile.id,
                        profile.firstName,
                        profile.lastName, null,
                        profile.getProfilePictureUri(200, 200).toString())
            }

            override fun onCancel() {}

            override fun onError(error: FacebookException) {
                input_layout_password.error = error.toString()
            }
        })
    }

    override fun showLoginValidError() {
        input_layout_login.error = getString(R.string.login_valid_error)
    }

    override fun showPasswordValidError() {
        input_layout_password.error = getString(R.string.password_valid_error)
    }

    override fun clearErrors() {
        input_layout_login.error = ""
        input_layout_password.error = ""
    }

    override fun onSuccesfulLogIn() {
        val intent = Intent(this, HomeActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        startActivity(intent)
    }

    override fun showUnknownError() {
        input_password.error = "unknown error"
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent) {
        super.onActivityResult(requestCode, resultCode, data)

        when (requestCode) {
            RC_SIGN_IN -> {
                val task = GoogleSignIn.getSignedInAccountFromIntent(data)
                handleSignInResult(task)
            }
            TwitterAuthConfig.DEFAULT_AUTH_REQUEST_CODE -> twitter_sign_in_button.onActivityResult(requestCode, resultCode, data)
            else -> fbCallbackManager.onActivityResult(requestCode, resultCode, data)
        }
    }

    private fun handleSignInResult(completedTask: Task<GoogleSignInAccount>) {
        try {
            val account = completedTask.getResult(ApiException::class.java)
            loginPresenter.saveUserAndGo(account.id, account.givenName,
                    account.familyName,
                    account.email,
                    if (account.photoUrl == null) null else account.photoUrl!!.toString())
        } catch (e: ApiException) {
            showAutocloseableMessage(e.message?:"")
        }

    }
}
