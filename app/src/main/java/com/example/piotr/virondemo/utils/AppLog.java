package com.example.piotr.virondemo.utils;

import android.app.Activity;
import android.app.Fragment;
import android.support.annotation.NonNull;
import android.util.Log;

import com.arellomobile.mvp.MvpAppCompatFragment;
import com.example.piotr.virondemo.BuildConfig;
import com.example.piotr.virondemo.ui.base.BasePresenter;

public abstract class AppLog {


    public static void logPresenter(@NonNull BasePresenter presenter){
        if (isLogEnabled()) {
            Log.i(getAppTag(), getInfo(presenter));
        }
    }

    public static void logActivity(@NonNull Activity activity){
        if (isLogEnabled()) {
            Log.i(getAppTag(), getInfo(activity));
        }
    }

    public static void logFragment(@NonNull MvpAppCompatFragment fragment){
        if (isLogEnabled()) {
            Log.i(getAppTag(), getInfo(fragment));
        }
    }

    private static boolean isLogEnabled(){
        return BuildConfig.DEBUG;
    }

    private static String getClassName(Object o) {
        return o.getClass().getName();
    }

    private static String getMethodName() {
        try {
            return Thread.currentThread().getStackTrace()[5].getMethodName();
        } catch (Exception e) {
            return "Unkhown method";
        }
    }

    private static String getAppTag() {
        return "AAA_TAG";
    }

    private static String getInfo(Object o){
        return "Class: "+getClassName(o)+"."+getMethodName() + "()";
    }
}
