package com.example.piotr.virondemo.utils

import android.app.Activity
import android.app.Fragment
import android.support.design.widget.BaseTransientBottomBar
import android.support.design.widget.Snackbar
import android.support.v4.content.ContextCompat
import android.view.View
import android.widget.TextView
import com.example.piotr.virondemo.R
import com.example.piotr.virondemo.ui.base.BaseFragment
import java.time.Duration

fun Activity.showSnackBar(rootView: View?,
                          message: String?,
                      actionText: String?,
                      actionListener: View.OnClickListener?,
                      @BaseTransientBottomBar.Duration duration: Int): Snackbar? {
    hideKeyboard()
    var snackbar: Snackbar? = null
    try {
        snackbar = Snackbar.make(rootView!!, message.toString(), duration)
    } catch (e: Exception) {
        e.printStackTrace()
    }

    if (snackbar != null) {
        if (actionListener != null) {
            snackbar.setAction(actionText, actionListener)
        }
        snackbar.setActionTextColor(ContextCompat.getColor(applicationContext!!, R.color.colorAccent))

        val snackBarView = snackbar.view
        snackBarView.setBackgroundColor(ContextCompat.getColor(applicationContext!!, R.color.color_gray_light))
        val textView = snackBarView.findViewById<TextView>(R.id.snackbar_text)
        textView.setTextColor(ContextCompat.getColor(applicationContext!!, R.color.color_primary_text))
        snackbar.show()
    }

    return snackbar
}

fun BaseFragment.showSnackBar(message: String?,
                          actionText: String?,
                          actionListener: View.OnClickListener?,
                          @BaseTransientBottomBar.Duration duration: Int): Snackbar? {
    activity?.hideKeyboard()
    var snackbar: Snackbar? = null
    if (activity != null && activity!!.applicationContext != null) {
        try {
            if (view != null) {
                snackbar = Snackbar.make(view!!, message.toString(), duration)
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }

        if (snackbar != null) {
            if (actionListener != null) {
                snackbar.setAction(actionText, actionListener)
            }
            snackbar.setActionTextColor(ContextCompat.getColor(activity!!.applicationContext!!, R.color.colorAccent))

            val snackBarView = snackbar.view
            snackBarView.setBackgroundColor(ContextCompat.getColor(activity!!.applicationContext!!, R.color.color_gray_light))
            val textView = snackBarView.findViewById<TextView>(R.id.snackbar_text)
            textView.setTextColor(ContextCompat.getColor(activity!!.applicationContext!!, R.color.color_primary_text))
            snackbar.show()
        }
    }
    return snackbar
}